## Perbedaan JSON dan XML

---

JSON atau Javascript Object Notation adalah format file yang berbentuk teks yang biasa digunakan dalam pertukaran data yang berisi pasangan atribut/key-nilai dan array. File JSON lebih mudah untuk dibaca karena JSON bersifat self-describing, sedangkan File XML bersifat self-desciprtive. Pada XML, kita dapat menambahkan komen yang mana hal tersebut tidak dapat kita lakukan dalam JSON. Sebagai Markup langauge XML memiliki kemampuan untuk menampilkan data, sedangkan JSON tidak bisa

## Perbedaan HTML dan XML

---

HTML atau HyterText Markup Language bersifat statis dan biasanya digunakan dalam penyajian data. Sedangkan XML, bersifat dinamis biasanya digunakan untuk pertukaran data. Dalam penulisannya, tag-tag pada HTML telah disediakan dan kita tidak harus selalu menggunakan tag penutup, berkebalikannya pada XML kita dapat membuat tag sendiri dan diwajibkan untuk menggunakan tag penutup.

## Sumber:

- https://www.geeksforgeeks.org/html-vs-xml/
- https://www.guru99.com/json-vs-xml-difference.html
