from lab_1.models import Friend
from django import forms


class DateInput(forms.DateInput):
    input_type = 'date'


class FriendForm(forms.ModelForm):
    class Meta:
        widgets = {'dob': DateInput()}
        model = Friend
        fields = '__all__'
        # kalo pake all ini ga bisa pake error messages & input_attrs & display name
        # kalo pake ini ['display_name'], fieldsnya bakal dihandle di HTML forms
