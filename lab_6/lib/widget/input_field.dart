import 'package:flutter/material.dart';
import 'package:lab_6/constants.dart';
import 'package:lab_6/widget/input_field_container.dart';

class InputFieldsC extends StatefulWidget {
  final String hintText;
  final bool hideValue;
  final IconData icon;
  final ValueChanged<String> onChanged;
  const InputFieldsC({
    Key? key,
    required this.hintText,
    required this.hideValue,
    required this.icon,
    required this.onChanged,
  }) : super(key: key);

  @override
  _InputFieldsCState createState() => _InputFieldsCState();
}

class _InputFieldsCState extends State<InputFieldsC> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return InputFieldContainer(
      child: TextField(
        style: TextStyle(color: Colors.grey.shade800),
        obscureText: widget.hideValue,
        onChanged: widget.onChanged,
        cursorColor: purpleMimosa,
        decoration: InputDecoration(
          icon: Icon(
            widget.icon,
            color: purpleMimosa,
          ),
          hintText: widget.hintText,
          hintStyle: TextStyle(color: Colors.grey.shade500),
          border: InputBorder.none,
        ),
      ),
    );
  }
}
