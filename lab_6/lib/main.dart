import 'package:flutter/material.dart';
import 'package:lab_6/constants.dart';
import 'package:lab_6/screens/login/login_page.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Covid Consult',
      theme: ThemeData(
        primarySwatch: Colors.grey,
        scaffoldBackgroundColor: charcoal,
        backgroundColor: charcoal,
      ),
      // ignore: prefer_const_constructors
      home: LoginScreen(),
    );
  }
}
