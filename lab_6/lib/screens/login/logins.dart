// ignore_for_file: prefer_const_literals_to_create_immutables, prefer_const_constructors, camel_case_types

import 'package:flutter/material.dart';
import 'package:lab_6/screens/signup/sign_up_page.dart';
import 'package:lab_6/widget/button_custom.dart';
import 'package:lab_6/widget/have_an_account_text.dart';
import 'package:lab_6/widget/input_field.dart';

class logins extends StatefulWidget {
  const logins({Key? key}) : super(key: key);

  @override
  _loginsState createState() => _loginsState();
}

class _loginsState extends State<logins> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(
          "Hello 🙋‍♀️",
          style: TextStyle(
              color: Colors.white, fontWeight: FontWeight.bold, fontSize: 24),
        ),
        SizedBox(
          height: size.height * 0.02,
        ),
        Text(
          "Welcome Back",
          style: TextStyle(
              color: Colors.white, fontWeight: FontWeight.bold, fontSize: 24),
        ),
        SizedBox(
          height: size.height * 0.03,
        ),
        InputFieldsC(
            hintText: "Username",
            hideValue: false,
            icon: Icons.person,
            onChanged: (value) {}),
        InputFieldsC(
            hintText: "Password",
            hideValue: true,
            icon: Icons.lock,
            onChanged: (value) {}),
        SizedBox(
          height: size.height * 0.03,
        ),
        buttons(text: "Login"),
        SizedBox(
          height: size.height * 0.03,
        ),
        AlreadyHaveAnAccountCheck(press: () {
          Navigator.push(context, MaterialPageRoute(builder: (context) {
            return SignUpScreen();
          }));
        })
      ],
    );
  }
}
