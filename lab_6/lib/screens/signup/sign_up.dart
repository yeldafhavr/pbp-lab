// ignore_for_file: prefer_const_literals_to_create_immutables, prefer_const_constructors, camel_case_types

import 'package:flutter/material.dart';
import 'package:lab_6/screens/login/login_page.dart';
import 'package:lab_6/screens/signup/sign_up_page.dart';
import 'package:lab_6/widget/button_custom.dart';
import 'package:lab_6/widget/have_an_account_text.dart';
import 'package:lab_6/widget/input_field.dart';

class signup extends StatefulWidget {
  const signup({Key? key}) : super(key: key);

  @override
  _signupState createState() => _signupState();
}

class _signupState extends State<signup> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 30),
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text(
              "✨ Hello ✨ ",
              style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 24),
            ),
            SizedBox(
              height: size.height * 0.02,
            ),
            Text(
              "Sign up to get started",
              style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 24),
            ),
            SizedBox(
              height: size.height * 0.03,
            ),
            InputFieldsC(
                hintText: "Name",
                hideValue: false,
                icon: Icons.person,
                onChanged: (value) {}),
            InputFieldsC(
                hintText: "Username",
                hideValue: false,
                icon: Icons.person,
                onChanged: (value) {}),
            InputFieldsC(
                hintText: "Email",
                hideValue: false,
                icon: Icons.email,
                onChanged: (value) {}),
            InputFieldsC(
                hintText: "Date of Birth",
                hideValue: false,
                icon: Icons.calendar_today,
                onChanged: (value) {}),
            InputFieldsC(
                hintText: "Phone Number",
                hideValue: false,
                icon: Icons.phone_android,
                onChanged: (value) {}),
            InputFieldsC(
                hintText: "Password",
                hideValue: true,
                icon: Icons.lock,
                onChanged: (value) {}),
            InputFieldsC(
                hintText: "Confirm Password",
                hideValue: true,
                icon: Icons.lock,
                onChanged: (value) {}),
            SizedBox(
              height: size.height * 0.03,
            ),
            buttons(text: "Login"),
            SizedBox(
              height: size.height * 0.03,
            ),
            AlreadyHaveAnAccountCheck(
                login: false,
                press: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return LoginScreen();
                  }));
                })
          ],
        ),
      ),
    );
  }
}
