from django.urls import path
from .views import index, add_note, note_list

app_name = "lab4"
urlpatterns = [
    path('', index, name='index'),
    path('add-note', add_note, name='formNote'),
    path('note-list', note_list, name='noteCard'),
]
