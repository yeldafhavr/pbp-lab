from lab_2.models import Note
from django import forms


class NoteForm(forms.ModelForm):
    class Meta:
        model = Note
        fields = '__all__'
        # kalo pake all ini ga bisa pake error messages & input_attrs & display name
        # kalo pake ini ['display_name'], fieldsnya bakal dihandle di HTML forms
