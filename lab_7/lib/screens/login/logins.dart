// ignore_for_file: prefer_const_literals_to_create_immutables, prefer_const_constructors, camel_case_types

import 'package:flutter/material.dart';
import 'package:lab_7/constants.dart';
import 'package:lab_7/screens/signup/sign_up_page.dart';
import 'package:lab_7/widget/button_custom.dart';
import 'package:lab_7/widget/have_an_account_text.dart';
import 'package:lab_7/widget/input_field.dart';

class logins extends StatefulWidget {
  const logins({Key? key}) : super(key: key);

  @override
  _loginsState createState() => _loginsState();
}

class _loginsState extends State<logins> {
  final _formKey = GlobalKey<FormState>();

  final snackBar = SnackBar(
    content: Text(
      'Login Successfully',
      textAlign: TextAlign.center,
    ),
    behavior: SnackBarBehavior.floating,
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(5),
    ),
    backgroundColor: purpleMimosa,
  );

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(
          "Hello 🙋‍♀️",
          style: TextStyle(
              color: Colors.white, fontWeight: FontWeight.bold, fontSize: 24),
        ),
        SizedBox(
          height: size.height * 0.02,
        ),
        Text(
          "Welcome Back",
          style: TextStyle(
              color: Colors.white, fontWeight: FontWeight.bold, fontSize: 24),
        ),
        SizedBox(
          height: size.height * 0.03,
        ),
        Form(
            key: _formKey,
            child: Column(
              children: <Widget>[
                InputFieldsC(
                    hintText: " ",
                    labelText: "Username",
                    hideValue: false,
                    icon: Icons.person,
                    errormsg: "Username cannot be left blank",
                    onChanged: (value) {}),
                InputFieldsC(
                    hintText: " ",
                    labelText: "Password",
                    hideValue: true,
                    icon: Icons.lock,
                    errormsg: "Password cannot be left blank",
                    onChanged: (value) {}),
              ],
            )),
        SizedBox(
          height: size.height * 0.03,
        ),
        buttons(
          text: "Login",
          onPressed: () {
            if (_formKey.currentState!.validate()) {
              print("omoya");
              ScaffoldMessenger.of(context).showSnackBar(snackBar);
            } else {
              print("null gan");
            }
          },
        ),
        SizedBox(
          height: size.height * 0.03,
        ),
        AlreadyHaveAnAccountCheck(press: () {
          Navigator.push(context, MaterialPageRoute(builder: (context) {
            return SignUpScreen();
          }));
        })
      ],
    );
  }
}
