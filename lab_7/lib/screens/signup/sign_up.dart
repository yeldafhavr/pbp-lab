// ignore_for_file: prefer_const_literals_to_create_immutables, prefer_const_constructors, camel_case_types

import 'package:flutter/material.dart';
import 'package:lab_7/screens/login/login_page.dart';
import 'package:lab_7/widget/button_custom.dart';
import 'package:lab_7/widget/have_an_account_text.dart';
import 'package:lab_7/widget/input_field.dart';
import 'package:lab_7/constants.dart';

class signup extends StatefulWidget {
  const signup({Key? key}) : super(key: key);

  @override
  _signupState createState() => _signupState();
}

class _signupState extends State<signup> {
  final _formKey = GlobalKey<FormState>();
  final snackBar = SnackBar(
    content: Text(
      'Account Created Successfully',
      textAlign: TextAlign.center,
    ),
    behavior: SnackBarBehavior.floating,
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(5),
    ),
    backgroundColor: purpleMimosa,
  );

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 30),
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text(
              "✨ Hello ✨ ",
              style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 24),
            ),
            SizedBox(
              height: size.height * 0.02,
            ),
            Text(
              "Sign up to get started",
              style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 24),
            ),
            SizedBox(
              height: size.height * 0.03,
            ),
            Form(
                key: _formKey,
                child: Column(
                  children: <Widget>[
                    InputFieldsC(
                        hintText: "Name",
                        labelText: "Name",
                        hideValue: false,
                        icon: Icons.person,
                        errormsg: "Name cannot be left blank",
                        onChanged: (value) {}),
                    InputFieldsC(
                        hintText: "Username",
                        labelText: "Username",
                        hideValue: false,
                        icon: Icons.person,
                        errormsg: "Username cannot be left blank",
                        onChanged: (value) {}),
                    InputFieldsC(
                        hintText: "Email",
                        labelText: "Email",
                        hideValue: false,
                        icon: Icons.email,
                        errormsg: "Email cannot be left blank",
                        onChanged: (value) {}),
                    InputFieldsC(
                        hintText: "Date of Birth",
                        labelText: "Date of Birth",
                        hideValue: false,
                        icon: Icons.calendar_today,
                        errormsg: "Date of Birth be left blank",
                        onChanged: (value) {}),
                    InputFieldsC(
                        hintText: "Phone Number",
                        labelText: "Phone Number",
                        hideValue: false,
                        icon: Icons.phone_android,
                        errormsg: "Phone number cannot be left blank",
                        onChanged: (value) {}),
                    InputFieldsC(
                        hintText: "Password",
                        labelText: "Password",
                        hideValue: true,
                        icon: Icons.lock,
                        errormsg: "Password cannot be left blank",
                        onChanged: (value) {}),
                    InputFieldsC(
                        hintText: "Confirm Password",
                        labelText: "Confirm Password",
                        hideValue: true,
                        icon: Icons.lock,
                        errormsg: "Confirm Password cannot be left blank",
                        onChanged: (value) {}),
                  ],
                )),
            SizedBox(
              height: size.height * 0.03,
            ),
            buttons(
              text: "Sign Up",
              onPressed: () {
                if (_formKey.currentState!.validate()) {
                  print("omoya");
                  ScaffoldMessenger.of(context).showSnackBar(snackBar);
                } else {
                  print("null gan");
                }
              },
            ),
            SizedBox(
              height: size.height * 0.03,
            ),
            AlreadyHaveAnAccountCheck(
                login: false,
                press: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return LoginScreen();
                  }));
                })
          ],
        ),
      ),
    );
  }
}
