// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:lab_7/constants.dart';

class buttons extends StatefulWidget {
  final String text;
  final VoidCallback onPressed;
  const buttons({
    Key? key,
    required this.text,
    required this.onPressed,
  }) : super(key: key);

  @override
  _buttonsState createState() => _buttonsState();
}

class _buttonsState extends State<buttons> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      width: size.width * 0.8,
      child: ElevatedButton(
        child: Text(
          widget.text,
          style: TextStyle(color: Colors.white70),
        ),
        onPressed: widget.onPressed,
        style: ElevatedButton.styleFrom(
            primary: purpleMimosa,
            padding: EdgeInsets.symmetric(horizontal: 40, vertical: 20),
            textStyle: TextStyle(
                color: Colors.white70,
                fontSize: 14,
                fontWeight: FontWeight.w500)),
      ),
    );
  }
}
