import 'package:flutter/material.dart';
import 'package:lab_7/constants.dart';

class InputFieldsC extends StatefulWidget {
  final String hintText;
  final bool hideValue;
  final IconData icon;
  final String errormsg;
  final String labelText;
  final ValueChanged<String> onChanged;
  const InputFieldsC({
    Key? key,
    required this.hintText,
    required this.labelText,
    required this.hideValue,
    required this.icon,
    required this.onChanged,
    required this.errormsg,
  }) : super(key: key);

  @override
  _InputFieldsCState createState() => _InputFieldsCState();
}

class _InputFieldsCState extends State<InputFieldsC> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 10),
      width: size.width * 0.8,
      child: TextFormField(
        // return TextFormField(
        style: TextStyle(color: Colors.grey.shade100),
        obscureText: widget.hideValue,
        autofocus: true,
        // onChanged: widget.onChanged,
        cursorColor: Colors.grey.shade100,
        validator: (value) {
          if (value!.isEmpty) {
            return widget.errormsg;
          }
          return null;
        },
        decoration: InputDecoration(
          fillColor: Colors.deepPurple.shade50,
          prefixIcon: Icon(
            widget.icon,
            color: purpleMimosa,
          ),
          labelText: widget.labelText,
          labelStyle: TextStyle(color: Colors.grey.shade100),
          floatingLabelStyle: const TextStyle(color: purpleMimosa),
          hintText: widget.hintText,
          hintStyle: const TextStyle(color: purpleMimosa),
          border: const OutlineInputBorder(),
          enabledBorder: const OutlineInputBorder(
            borderSide: BorderSide(color: Colors.white, width: 1.0),
          ),
          focusedBorder: const OutlineInputBorder(
            borderSide: BorderSide(color: purpleMimosa, width: 1.0),
          ),
        ),
      ),
    );
  }
}
