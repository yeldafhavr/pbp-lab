from django.http.response import HttpResponse
from django.core import serializers
from .models import Note
from django.shortcuts import render

# Create your views here.


def index(request):
    notes = Note.objects.all()
    response = {'notes': notes}
    return render(request, 'lab2.html', response)

# views tu selalu nerima argumen, walaupun ga dipake dimana" di dlm function ini dia tetep harus ada parameternya


def xml(request):
    notes = Note.objects.all()
    data = serializers.serialize('xml', notes)
    return HttpResponse(data, content_type="application/xml")


def json(request):
    notes = Note.objects.all()
    data = serializers.serialize('json', notes)
    return HttpResponse(data, content_type="application/json")
